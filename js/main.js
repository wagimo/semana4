

	$(function () {
		$('[data-toggle="tooltip"]').tooltip();
		$('[data-toggle="popover"]').popover();
		$('.carousel').carousel({
			interval:5000
		});
	});


// LOGS EVENTOS MODALES

$("#exampleModal").on("show.bs.modal", function(){
	console.log("Abriendo Modal...");
});
$("#exampleModal").on("shown.bs.modal", function(){
	console.log("Modal Abierto");
	$(".contacto").removeClass("btn-primary");
	$(".contacto").addClass("btn-danger");
	$(".contacto").prop("disabled", true);
});
$("#exampleModal").on("hide.bs.modal", function(){
	console.log("Cerrando Modal...");
});
$("#exampleModal").on("hidden.bs.modal", function(){
	console.log("Modal Cerrado");

	$(".contacto").removeClass("btn-danger");
	$(".contacto").addClass("btn-primary");
	$(".contacto").prop("disabled", false);
});
